/*
 * rfid.h
 *
 *  Created on: 22 feb. 2023
 *      Author: VersD
 *
 *  based on https://github.com/miguelbalboa/rfid
 */

#ifndef LIB_RFID_H_
#define LIB_RFID_H_
#include "spi.h"

void rfidInitialize(uint8_t chipselect, uint8_t reset);

int8_t rfidGetBlok(uint8_t blokNummer, uint8_t bytes[16]);

void rfidEnableInterrupt();
void rfidClearInterrupt();
void rfidWake();

#endif /* LIB_RFID_H_ */
