

#ifndef LIB_SPI_H_
#define LIB_SPI_H_

#include <stdint.h>

void spiInitialize(uint8_t module);

uint8_t spiSendByte(uint8_t data);

void spiStop();

#endif
