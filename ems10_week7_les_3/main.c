#include <msp430.h> 
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"
#include "lib/temperatuur.h"
#include "lib/rfid.h"
#include "lib/spi.h"

volatile uint8_t sysTickFlag=0;

//Alleen nodig als je de msp430 als i2c slave wilt instellen
//normaal is master het enige wat je nodig hebt.
//void i2cSlaveTransmitISR(void){}
//void i2cSlaveReceiveISR(void){}

/**
 * \mainpage MSP430G2553 Library
 *
 * Deze libary dient ter ondersteuning van het lesmateriaal bij
 * EMS10. De library is niet compleet maar voorziet in
 * de benodigde functionaliteiten voor het aansturen van de
 * meegeleverde OLED Display.
 *
 * Alle informatie is te vinden in bij de modules
 * - Er zijn functies om de GPIO in te stellen
 * - De i2c bus is beperkt geimplementeerd, alleen de master modus
 * - Het kloksysteem heeft een aantal functies
 * - En het belangrijkste, een aantal functionaliteiten van het display zijn ook geimplementeerd.
 *
 */

//een pagebuffer van 2 rijen hoog. 
uint8_t pageBuffer[2][128] = {0};

const uint8_t sine[] =
{
 0x7,0x8,0xa,0xb,0xc,0xd,0xd,0xe,
 0xe,0xe,0xd,0xd,0xc,0xb,0xa,0x8,
 0x7,0x6,0x4,0x3,0x2,0x1,0x1,0x0,
 0x0,0x0,0x1,0x1,0x2,0x3,0x4,0x6,
};

#pragma vector = TIMER0_A1_VECTOR
__interrupt void sysTick(void){
    TA0CTL &= ~TAIFG;

    //nu main loop 1 maal uitvoeren voor we weer gaan slapen
    //tot de volgende timer interrupt
    __low_power_mode_off_on_exit();
}

void timerInit(){
    TA0CTL = TASSEL_2 | ID_3 | MC_1 | TAIE;
    // 16MHz/8 clock, 2000->1ms ticks (0...19999) -> 50Hz/20ms interrupt
    TA0CCR0 = 39999;
}

void printSinus()
{
    static uint8_t n=0;
    uint8_t kolom;

    n++;
    n=n%32;

    for(kolom=0; kolom<128;kolom++)
    {
        oledSetBufferPixel(pageBuffer, kolom,sine[(kolom+n+1)%32]);
        oledClearBufferPixel(pageBuffer, kolom,sine[(kolom+n)%32]);
    }
    oledWriteBuffer(pageBuffer, 3,2);
}

//Dit stukje code leest een i2c temperatuur
//sensor uit.
//zie https://www.farnell.com/datasheets/2794261.pdf
void printTemperatuur()
{
    char temp[6];
    int16_t t;
    uint8_t data[2];

    //selecteer register 0 en ontvang de 2 bytes
    //i2cReceiveFromRegister(0x48, 0x00, 2, data);
    i2cReceiveBytes(0x48, 2, data);

    //samenvoegen
    t=(data[1] <<8|data[0]);
    //12 bit dus verschuiven
    t >>= 4;
    //0.0625 graden per bit
    itoa((int)(t*0.625),temp);

    oledClearBox(70, 1, 100, 1);
    oledPrint(70,1, temp, small);
}

bool isAantalTicks(uint16_t teller, uint16_t ticks)
{
    return (teller%ticks)==0;
}

void main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */


    i2cInitialize(MASTER);
    spiInitialize(0);
    rfidInitialize(BIT3,BIT0);

    oledInitialize();
    oledSetOrientation(FLIPPED);
    oledClearScreen();
    oledPrint(20,1, "Temp: ", small);
    oledPrint(40,6, "sin(w*t)", small);


    timerInit();
    __enable_interrupt();

    uint16_t tijd=0;
    while(1){


        if (isAantalTicks(tijd, 1)) //20ms
        {
            printSinus();
        }

        if (isAantalTicks(tijd, 5)) //100ms
        {
            int8_t ret=0;
            uint8_t blok[16];
            ret = rfidGetBlok(9, blok);
            oledClearBox(40, 2, 40+7*5, 2);
            if (ret >= 0)
            {
                oledPrint(40,2, "Succes!", small);
            }else{
                oledPrint(40,2, "Niets..", small);
            }


        }

        if (isAantalTicks(tijd, 50)) //1s
        {
            //printTemperatuur();
        }

        tijd++;
        tijd%=50; //reset bij max.

        //Nu gaan slapen. Timer loopt door.
        __low_power_mode_1();

	}

}
